<?php

namespace App\Library;


class WorkManager {

    protected $Password = 'Password';
    protected $User = 'User';
    protected $Token;
    protected $FormCode = 'FormCode';
    protected $header;

    function __construct()
    {
        $this->Token = $this->getToken();
        $this->header = ['User' => $this->User, 'Token' => $this->Token];
    }

    public function FormGetData($document){
        $url = 'http://{host}:{port}/api/Forms/Form_GetData';
        $filter = [
            'Field'     => 'document',
            'Operator'  => '=',
            'Value'     => $document
        ];
        $FilterParameters[] = $filter;
        $filter = [
            'Field'     => 'Anio',
            'Operator'  => '=',
            'Value'     => date('Y')
        ];
        $FilterParameters[] = $filter;
        $data = [
            'FormCode'  => $this->FormCode,
            'Header'    => $this->header,
            'FilterParameters' => $FilterParameters,

        ];
        // dd($data);
        $response['status'] = false;
        return $response;
    }

    public function FormInsert($data){
        $url = 'http://{host}:{port}/api/Forms/Form_Insert';
        $data['OfficeCode'] = 'OfficeCode';
        $data['FormCode'] = $this->FormCode;
        $data['Header'] = $this->header;
        $response = [
            'Success' => true,
            'Message' => 'Error del sistema',
            'BarCode' => 'ACR545Kloi8',
        ];
       return  $response;

        return $this->curlSetop($data, $url);

    }

    public function getToken(){
        $url = 'http://{host}:{port}/api/Token/GetToken';
        $data = [
            'Password' => $this->Password,
            'User' => $this->User,
        ];

        $Token = rand();

        return $Token;
    }

    private function curlSetop($data, $url){
        $json_data = json_encode($data);
        $token = 'DZjNerATbj7T88kMpMqjuL';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('accept: application/json',
               'Authorization: ' . $token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
        $response = curl_exec($curl);
        $response = json_decode($response, true);

        return $response;
    }


}
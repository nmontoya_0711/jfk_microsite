<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use App\Library\{Recaptcha, WorkManager};
use Illuminate\Http\Request;
use \DrewM\MailChimp\MailChimp;
use Illuminate\Support\Facades\Session;
use App\Models\Register;
use Mail;
use View;


class FrontController extends Controller {

     public function home(Request $request){
          $type = '';
          $message = '';
          if (session()->has('type')) {
               $type = session()->get('type');
               $message = session()->get('message');
               session()->forget('type');
               session()->forget('message');
          }
          // dd($type);
          return view('pages.home', compact('type', 'message'));
     }

     public function send_documents(Request $request){
          $message = __('messages.data_send');
          $type = 'success';
          $benefiteds = [
               'self' => 'Yo mismo',
               'spouse' => 'Cónyuge',
               'permanent_companion' => 'Compañero permanente',
               'son' => 'Hijo',
               'brother' => 'Hermano',
               'grandchild' => 'Nieto',
               'fathers' => 'Padres'
          ];
          if ($request->isMethod('POST')){
               $data_request = $request->all();
               $document = $data_request['document'];
               $record = Register::whereDocument($document)->first();
               if ($record) {
                    $verifyDocumentWorkManager = (new WorkManager)->FormGetData($document);//Valida que el documento no este ingresado en el WorkManager
                    if (!$verifyDocumentWorkManager['status']) {//Se puede continuar con el proceso                 
                         $benefited = $benefiteds[$data_request['benefited']];   
                         $file_1 = $data_request['file_1'];                    
                         $file_2 = isset($data_request['file_2']) ? $data_request['file_2'] : NULL;
                         $file_3 = isset($data_request['file_3']) ? $data_request['file_3'] : NULL;

                         $Files[] = $this->getFileDto('file_1', $file_1, $data_request['benefited']);
                         if ($file_2) {
                             $Files[] = $this->getFileDto('file_2', $file_2, $data_request['benefited']);
                         }
                         if ($file_3) {
                             $Files[] = $this->getFileDto('file_3', $file_3, $data_request['benefited']);
                         }

                         $InsertRequestDto['OperationUser'] = $document;
                         $InsertRequestDto['Data'] = $this->getData($record, $benefited);
                         $InsertRequestDto['Files'] = $Files;
                         // dd($InsertRequestDto);
                         $FormInsert = (new WorkManager)->FormInsert($InsertRequestDto);
                         if (!$FormInsert['Success']) {
                              $message = $FormInsert['Message'];
                              $type = 'error';
                         }else{
                              $message .= '<br> El número del radicado de los documentos es <strong style="color:#643c8d">' . $FormInsert['BarCode'] . '</strong>';
                         }
                    }else{//Ya este afiliado readicó los documentos
                         $message = __('messages.verify_fail') . ' <strong style="color:#643c8d">' . date('Y-m-d') . '</strong>';
                         $type = 'error';
                    }
                    // if(!Recaptcha::validate($data['_recaptcha'])){
                    //        $message = __('messagesa.invalid_recaptcha');
                              //$type = false;
                    // }else{
                    //      $file_1 = $data['file_1'];
                    //      $file_2 = $data['file_2'];
                    //      $file_3 = $data['file_3'];
                    //      $benefited = $benefiteds[$data['benefited']];
                    // }
               }else{
                    $message = __('messages.error_document');
                    $type = 'error';//El documento ya no es válido
               }
               session()->put('message', $message);
               session()->put('type', $type);
          }    
          return redirect()->route('home');
     }

     public function ax_validate_document(){
          if (request()->ajax()) {
            $data = request()->all();
            $document = $data['document'];
            $status = false;
            $message = __('messages.document_not_register');
            $name = '';
            $record = Register::whereDocument($document)->first();
            if ($record) {
                $status = true;
                $name = $record->name;
            }
            return response()->json(['status' => $status, 'message' => $message, 'name' => $name]);
        }

    }  

    private function getData($record, $benefited){
          $data[] = [
               'Field' => 'document',
               'Value' => $record->document
          ];
          $data[] = [
               'Field' => 'name',
               'Value' => $record->name
          ];
          $data[] = [
               'Field' => 'email',
               'Value' => $record->email
          ];
          $data[] = [
               'Field' => 'mobile',
               'Value' => $record->mobile
          ];
          $data[] = [
               'Field' => 'benefited', 
               'Value' => $benefited
          ];
          $data[] = [
               'Field' => 'Anio', 
               'Value' => date('Y')
          ];

          return $data;
    }

    private function getFileDto($nameFile, $file, $benefited){
          $extension = $file->getClientOriginalExtension();
          $FileDto = [
               'Base64String' => base64_encode($file),
               'Ext' => '.'.$extension,
               'Description' => $this->getDescriptionFile($nameFile, $benefited),
               'DocumentTypeCode' => 'DocumentTypeCode',
               'DirectoryCode' => 'DirectoryCode'
          ];

          return $FileDto;
    }

     private function getDescriptionFile($file, $benefited){
          $description = 'Certificado de estudio expedido por la institución educativa o colilla de pago.';
          switch ($benefited) {
               case 'spouse':
                    if ($file == 'file_2') {
                         $description = 'Registro de Matrimonio o Partida de Matrimonio.';
                    }
                    break;
               case 'permanent_companion':
                    if ($file == 'file_2') {
                         $description = 'Certificación de la unión marital de hecho.';
                    }
                    break;
               case 'son':
                    if ($file == 'file_2') {
                         $description = 'Registro Civil de Nacimiento con nombre completo de los padres o Partida de Bautismo que demuestre la patria potestad.';
                    }
                    break;
               case 'brother':
                    if ($file == 'file_2') {
                         $description = 'Registro Civil de Nacimiento o partida de Bautismo del asociado.';
                    }elseif($file == 'file_3') {
                         $description = 'Registro Civil de Nacimiento o partida de Bautismo del hermano.';
                    }
                    break;
               case 'grandchild':
                    if ($file == 'file_2') {
                         $description = 'Registro Civil de Nacimiento o Partida de Bautismos del nieto, con el nombre de los padres.';
                    }elseif($file == 'file_3') {
                         $description = 'Registro Civil o Partida de Bautismo del hijo, quien a su vez es el padre del nieto.';
                    }
                    break;
               case 'fathers':
                    if ($file == 'file_2') {
                         $description = 'Registro Civil de Nacimiento o Partida de Bautismo del asociado.';
                    }elseif($file == 'file_3') {
                         $description = 'Fotocopia de la cédula del padre. ';
                    }
                    break;
          }

          return $description;

    }
}


<?php

return [

    'facebook' => 'https://www.facebook.com',
    'vimeo' => 'https://vimeo.com/user92022260',
    'instagram' => null,
    'pinterest' => 'https://www.pinterest.es/perceptualco/',
    'twitter' =>  null,
    'youtube' =>  'https://www.youtube.com/',    
    'linkedin' =>  'https://www.linkedin.com/feed/',
    'whatsapp' => '573165533082',
    'message_whatsapp' => 'Hola, necesito asesoria',

    'email_contact'=>'test.developer.nelson@gmail.com',
    'email_course'=> 'test.developer.nelson@gmail.com',
    'email_newsletter'=>'test.developer.nelson@gmail.com',
    'email_work_us'=>'test.developer.nelson@gmail.com',
    'email_credit_saving'=>'test.developer.nelson@gmail.com',
    'email_saving_simulator'=>'test.developer.nelson@gmail.com',
    'email_credit_simulator'=>'test.developer.nelson@gmail.com',
    'email_pqrs'=>'test.developer.nelson@gmail.com',
    'email_data_update'=>'test.developer.nelson@gmail.com,nelson.m1107@gmail.com',
    'email_ethical_line'=>'test.developer.nelson@gmail.com,nelson.m1107@gmail.com',

    'info_agency_antioquia'=>'<h3>Antioquia:</h3>
<p>Lunes a viernes: 8:00am a 3:00pm <br /> <br /> S&aacute;bados: <br /> 8:00am a 12:30 pm</p>',
    'info_agency_bogota'=>'<h3>Bogot&aacute;:</h3>
<p>Lunes a viernes: <br /> 8:30am a 3:00pm <br /> <br /> S&aacute;bados 8:30am a 12:30pm</p>',
      'info_footer'=>'<h3>Cont&aacute;ctenos</h3>
<p>Carrera 51 # 43 - 24 - Edificio Alpujarra <br /> Medell&iacute;n Colombia</p>
<p>Tel&eacute;fono: 262 64 44</p>
<p>Email: corporativo@jfk.com.co</p>',

    'info_email'=>null,
    'recaptcha_key_site'=>'6LewWx4aAAAAAFM8fHTQFATLUYviq1uS3ZtXHLKl',
    'recaptcha_key_secret'=>'6LewWx4aAAAAAJTTLrNgbrywbdsfqtQx0LMjY7d9',

    'url_google_maps'=>'https://goo.gl/maps/Tiec67RLt2FYYdjBA',
    'url_newsletter'=>'http://eepurl.com/glfamn',
    'code_analytics'=>null,
    'mc_api_key'=>'d08febf3711593cb7e12d171bb793ec1-us15',
    'mc_list_id'=>'1b55d5012d',
    'doc_policy'=>'files/document-test-pdf-0.pdf',
    'doc_guarantee'=>'files/o3jFOCNi1hwqw8cCnVowjDFw17WbPqaDfjvZlTNb.pdf',
    'doc_catalog'=>'files/VmxCHK4JquFLg4hNFfpcpVhmE9v76sHNfWZUsOSQ.pdf',
];
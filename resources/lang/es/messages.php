<?php
     return array (
     'document_not_register' => 'No se encuentra registrado en los auxilios educativos',
     'invalid_recaptcha' => 'REcaptcha Inválido',
     'data_send' => 'Sus documentos se han enviado satisfactoriamente para la reclamación del Auxilio Educativo, en caso de que JFK requiera información adicional, nos comunicaremos con usted a través de los datos contacto que registraste en la Cooperativa.',
     'verify_fail' => 'Sus documentos ya fueron radicados el',
     'error_document' => 'El documento ingresado no es correcto'
);
@extends('layouts.master')

@section('content')

<section class="banner">

    <div class="bnr_img">

        <img src="{{ asset('img/banner.jpg') }}" alt="">

    </div>

    <div class="bnr_txt">

        <h2>Auxilios Educativos</h2>

    </div>

</section>

<section class="form_1">
    @include('_partials.step_1')
    @include('_partials.step_2')    
</section>
@endsection

@include('_partials.script_recaptcha', array('id_form' => 'formDocument', 'message_send' =>  __('messages.send_email'), 'action' => 'auxilios_educativos'))


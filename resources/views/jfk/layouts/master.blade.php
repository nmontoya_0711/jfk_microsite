}<!DOCTYPE html>

<html lang="{{ config('app.locale') }}" >
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=0" />
    <title>{{ isset($meta_title) ? $meta_title : config('app.name') }}</title>
    <meta name="description" content="{{ isset($meta_description) ? $meta_description : '' }}">
    <meta name="keywords" content="{{ isset($meta_keywords) ? $meta_keywords : '' }}">
    <link rel="shortcut icon" href="{{ asset('img/favicons/favicon.ico') }}">
    <meta name="apple-mobile-web-app-title" content="JFK">
    <meta name="application-name" content="JFK">
    <meta name="msapplication-TitleColor" content="#000">
    <meta name="theme-color" content="#643c8d">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="base-url" content="{{ url('/') }}" />
    @yield('metas')
    <style type="text/css">
       [v-cloak] {
          display: none;
      } 
    </style>
    <script>
        window.paceOptions = {
            restartOnRequestAfter: false
        }
    </script>   
    <link type="text/css" rel="stylesheet" href="{{ asset('css/styles.css') }}" /> 
    <link type="text/css" rel="stylesheet" href="{{ asset('css/my_styles.css') }}" /> 
    <link type="text/css" rel="stylesheet" href="{{ asset('css/sweet_alert.css') }}" />  

    @stack('css')

    <script src="https://www.google.com/recaptcha/api.js?render={{ config('settings.recaptcha_key_site') }}"></script>

    {{-- @include('_partials.analytics') --}}
  </head>

  <body id="body">
    <div class="loader"><div class="spinner"></div></div>
    @include('_partials.header')
        @yield('content')
    @include('_partials.footer') <!-- FOOTER -->   
  
    <script>
        var baseRoot = "{{ url('/')}}";
        var hereUrl = '{{ request()->url() }}';
        var token = "{{ csrf_token() }}";
        var title_not_file = 'Revisa nuevamente los documentos anexos, puede que falte ingresar alguno.'
        var size_file = "El archivo no puede pesar mas de 2 MB";
        var mimes_file = "Solo se permiten archivos PDF y JPEG";
        
    </script>

    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('js/my_scripts.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    
    @stack('js')
  </body>

</html>




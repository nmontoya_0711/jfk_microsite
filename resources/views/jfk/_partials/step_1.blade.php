<div class="form_1_cnt" id="step_1">

        <h5>Ingresa tu número de cédula</h5>
        <section class="form-document">
            <div class="_input">
                <input type="text" id="document" placeholder="Número de cédula*" autocomplete="off">
            </div>
            <div class="_bottom">
                <div class="_check">
                    <input type="checkbox" name="" id="check_policy" checked>
                    <p>Al continuar manifiesto que he leído y acepto la <a href="{{ asset('files/politica-tratamiento-datos.pdf') }}" target="_blank"><strong>Política de Tratamiento de Datos</strong></a> de la Cooperativa y autorizo a JFK para recolectar, almacenar los datos que suministré y me contacte por los medios que informé.</p>
                </div>
                <div class="_submit">
                    <button type="button" class="btn" id="validateDocument">Continuar</button>
                </div>
            </div>
        </section>
    </div>
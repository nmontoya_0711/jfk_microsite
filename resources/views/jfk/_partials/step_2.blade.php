
<div class="form_1_cnt" id="step_2" style="display: none;">
    <h1 id="name-client">Juan Carlos Muñoz Marín</h1>
    <h5>Diligencia los siguientes campos</h5>
    <p>Indícanos quién es el beneficiario de Auxilio Educativo y anexa los documentos solicitados. Solo se admite PDF y JPEG.</p>
    <form action="{{ route('send.documents') }}" id="formDocument" class="form-load"  method="POST"  enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="_input">
            <select name="benefited" id="select_form">
                <option value="self">Auxilio educativo / Yo mismo</option>
                <option value="spouse">Auxilio educativo / Cónyuge</option>
                <option value="permanent_companion">Auxilio educativo / Compañero permanente</option>
                <option value="son">Auxilio educativo / Hijo</option>
                <option value="brother">Auxilio educativo / Hermano</option>
                <option value="grandchild">Auxilio educativo / Nieto</option>
                <option value="fathers">Auxilio educativo / Padres</option>
            </select>
        </div>
        <div class="files_content">
            <div class="_file">
              <p>Certificado de estudio expedido por la institución educativa o colilla de pago.</p>
              <label for="file_1">
                  Seleccionar archivo
                  <input type="file" name="file_1" id="file_1" data-file="file_1">
              </label>
              <span id="name-file_1">No se eligió archivo</span>
            </div>
            <div class="_file file_2">
              @include('pages.titles.spouse')
              @include('pages.titles.permanent_companion')
              @include('pages.titles.son')
              @include('pages.titles.brother')
              @include('pages.titles.grandchild')
              @include('pages.titles.fathers')
              <label for="file_2">
                  Seleccionar archivo
                  <input type="file" name="file_2" id="file_2" data-file="file_2">
              </label>
              <span id="name-file_2">No se eligió archivo</span>
            </div>
            <div class="_file file_3">
              @include('pages.titles.brother_1')
              @include('pages.titles.grandchild_1')
              @include('pages.titles.fathers_1')
              <label for="file_3">
                  Seleccionar archivo
                  <input type="file" name="file_3" id="file_3" data-file="file_3">
              </label>
              <span id="name-file_3">No se eligió archivo</span>
            </div>
        </div>
        <div class="_bottom">
            <div class="_check">
                {{-- <input type="checkbox" name="" id="" checked>
                <p>Al continuar manifiesto que he leído y acepto la <a href="{{ asset('files/politica-tratamiento-datos.pdf') }}" target="_blank"><strong>Política de Tratamiento de Datos</strong></a> de la Cooperativa y autorizo a JFK para recolectar, almacenar los datos que suministré y me contacte por los medios que informé.</p> --}}
                <button type="button" class="btn" id="back">Regresar</button>
            </div>
            <div class="_submit">
                <button type="button" class="btn" id="sendDocuments">Enviar documentos</button>
            </div>
        </div>
        <input type="hidden" name="_recaptcha" id="recaptcha">
        <input type="hidden" name="document" id="document-hidden">
    </form>
</div>


@push('js')
@if ($type != '')
  <script type="text/javascript">        
    swal({
        title: '{!! $message !!}',
        html : true,
        type: '{{ $type }}',
        confirmButtonText: "Cerrar",
        confirmButtonColor: '#643c8d'
     })
  </script>
@endif
<script>

     let action = '{{ $action }}';

     grecaptcha.ready(function () {

        grecaptcha.execute('{{  config('settings.recaptcha_key_site') }}', {

            action : action

        }).then(function(token){

              if(token){

                   document.getElementById('recaptcha').value = token;

              }

         });



        

     });



     //Every 90 Seconds

    setInterval(function () {

       grecaptcha.ready(function () {

          grecaptcha.execute('{{  config('settings.recaptcha_key_site') }}', {action: action}).then(function (token) {

             document.getElementById('recaptcha').value = token;

          });

        });

    }, 90 * 1000);

  </script>

@endpush
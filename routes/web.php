<?php



//Rutas autenticación usuario

// Auth::routes();



/*

|--------------------------------------------------------------------------

| Rutas traducidas

|--------------------------------------------------------------------------

 */

Route::group(['prefix' => LL::setLocale(), 'middleware' => ['localizationRedirect', 'web' , 'xss']], function () {

    Route::match(['get', 'post'], '/', 'FrontController@home')->name('home');   
    Route::post('send-documents', 'FrontController@send_documents')->name('send.documents');   
    Route::post('/ax-validate-document', 'FrontController@ax_validate_document');

});



//AJAX

Route::group(['prefix' => 'ajax', 'middleware' => [ 'web' , 'xss']], function () {



    Route::post('send-contact', 'FrontController@send_contact');

    Route::get('/', 'FrontController@home');

    Route::post('get-terms-saving', 'SavingSimulatorController@get_terms');

    Route::post('get-terms-mi-plan', 'SavingSimulatorController@get_terms_plan');

    Route::post('calculate-saving', 'SavingSimulatorController@calculate_saving');



    Route::post('get-terms-credit', 'CreditSimulatorController@get_terms');

    Route::post('get-terms-credit-fee', 'CreditSimulatorController@get_terms_monthly_fee');

    Route::post('calculate-credit', 'CreditSimulatorController@calculate_credit');



});

$(document).ready(function (){
    window.onload=show();

    function show(){
      $('.loader').fadeOut();
    }
    

    var colorBtn = '#643c8d';
    var file_1 = '';
    var file_2 = '';
    var file_3 = '';
    var option = '';
    var doc = '';

    $('#document').keyup(function(event) {
      event.preventDefault();
      if(event.which === 13){
        if(validateInput($(this)) & validateRadio($('#check_policy'))){ 
          execute_ajax();
        } 
      }      
    });

    $('#validateDocument').click(function(event) {
      if(validateInput($('#document')) & validateRadio($('#check_policy'))){ 
        execute_ajax();
      }   
    });
    $('#select_form').change(function(event) {
      $('.loader').fadeIn();
      option = $(this).val();
      setTimeout(function() {   
        if (option == 'self'){
          $('.file_2').css({display: 'none'});
          $('.file_3').css({display: 'none'});
        }else{       
          $('.file_2').css({display: 'inline-flex'});
          $('.title-input').css({display: 'none'});
          $('#'+option).css({display: 'inline-flex'});
          if (option == 'brother' || option == 'grandchild' || option == 'fathers') {
            $('.file_3').css({display: 'inline-flex'});
            $('#'+option+'_1').css({display: 'inline-flex' });
          }else{
            $('.file_3').css({display: 'none'});
          }
        }
      }, 500);    
      setTimeout(function() {$('.loader').fadeOut();}, 500);
    }); 

    function execute_ajax(){
      doc = $('#document').val();
      $.ajax({
          url : baseRoot +'/ax-validate-document',
          type : 'POST',
          dataType : 'json',
          data : {document : doc, _token : token},
          beforeSend: function () {
            $('.loader').fadeIn();
          },
          success : function(response){            
              if (response.status) {
                $('.form_1_cnt').fadeOut();
                $('#step_2').fadeIn();
                $('#document-hidden').val(doc);
                $('#name-client').text(response.name)
                setTimeout(function() {$('.loader').fadeOut();}, 500);
              }else{
                  $('.loader').fadeOut();
                  swal({
                     title: response.message,
                     type: "error",
                     confirmButtonText: "Cerrar",
                     confirmButtonColor: colorBtn
                  });
              }
           }
        });
    } 

    $('#back').click(function(event) {
      $('.loader').fadeIn();
      setTimeout(function() {
        $('.form_1_cnt').fadeOut();
        $('#step_1').fadeIn();
      }, 300);
      
      setTimeout(function() {$('.loader').fadeOut();}, 700);
    });

    $('#file_1').change(function(event) {
      if(validateFile($(this))){
        file_1 = $(this).val();
      }else{
        file_1 = '';
      }
    });

  $('#file_2').change(function(event) {
    if(validateFile($(this))){
      file_2 = $(this).val();
    }else{
      file_2 = '';
    }
  });

  $('#file_3').change(function(event) {
    if(validateFile($(this))){
      file_3 = $(this).val();
    }else{
      file_3 = '';
    }
  });   

  $('#sendDocuments').click(function(event) {
    var sw = true;
    if(option == 'self' || option == ''){
      if (file_1 == '') {
        sw = false;
      }
    }else if(option == 'spouse' || option == 'permanent_companion' || option == 'son'){
      if (file_1 == '' || file_2 == '') {
        sw = false;
      }
    }else{
      if (file_1 == '' || file_2 == '' || file_3 == '') {
        sw = false;
      }
    }

    if (!sw) {
      swal({
         title:title_not_file,
         type: "error",
         confirmButtonText: "Cerrar",
         confirmButtonColor: colorBtn
       });
      return false;
    }
    $('.loader').fadeIn();
    $('#formDocument').submit();
  });


    function validateInput(object){
      let valid = true;
      if(object.val() == '' || object.val() == null){
           valid = false;
           object.removeClass('is-valid1');
           object.addClass('not-valid');
      }else{   
           object.removeClass('not-valid');
           object.addClass('is-valid1');
      }
      return valid;
    }  

    function validateRadio(object){
      let valid = true;
      if (!object.is(':checked')){
         valid = false;
         object.removeClass('is-valid1');
         object.addClass('not-valid')
      }else{
          object.removeClass('not-valid');
          object.addClass('is-valid1');
      }
      return valid;

    }



     function validateFile(object){
          let valid = true;
          var id_file = object.data('file');
          if(object.val() == '' || object.val() == null){
               valid = false;
               object.removeClass('is-valid1');
               object.addClass('not-valid');
          }else{
               var file = object.val();
               var fileSize = object[0].files[0].size;
               var nameFile = object[0].files[0].name;
               var sizeMegaBytes = (fileSize / 1024) / 1024;
               title_swal = '';
               if (sizeMegaBytes > 2) {
                    title_swal = size_file + '\n';
               }
               var ext = file.slice((file.lastIndexOf(".") - 1 >>> 0) + 2);
               var array = ['pdf', 'PDF', 'jpg', 'jpeg']
               if (jQuery.inArray( ext, array) == -1) {
                  title_swal += mimes_file + '\n'; 
               }
               if (title_swal != '') {
                    swal({
                       title:title_swal,
                       type: "error",
                       confirmButtonText: "Cerrar",
                       confirmButtonColor: colorBtn
                     });
                  object.val('');
                  $('#name-'+id_file).text('No se eligió archivo');
                  valid = false;
                  object.removeClass('is-valid1');
                  object.addClass('not-valid');
               }else{
                  $('#name-'+id_file).text(nameFile);
                  object.removeClass('not-valid');
                  object.addClass('is-valid1');
               }
          }
          return valid;
     }

});

      

      

      

      

      
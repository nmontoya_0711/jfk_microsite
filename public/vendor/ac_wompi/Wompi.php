<?php

namespace App\Modules\ac_wompi;



use App\NovaCart\AbstractClass\Gateway;



class Wompi extends Gateway {



    public $code = 'ac_wompi';

    private $urlWompi= "https://checkout.wompi.co/p/";

   



    public function __construct() {

        $this->field_settings();

    }



    public function getData() {



        $params = $this->module->getSettings();

        

        $confirmationUrl = route('confirm_pay', $this->code);
        // $confirmationUrl = route('thancks');

        $test = $params->test;



        $amount = intval(str_replace(".", "", $this->order->grand_total));

        $public_key = $test ? $params->key_test : $params->key_prod;

        $dataWompi = array(

            'public-key' => $public_key,

            'reference' => $this->order->reference,

            'amount-in-cents' => $amount,

            'currency' => 'COP',

            'redirect-url' => $confirmationUrl,

        );

        $urlPay = $this->urlWompi;



        $data['type'] = 'checkout';

        $data['url_pay'] = $urlPay;

        $data['fields'] = $dataWompi;

        // dd($data);exit;

        return $data;

    }



    public function field_settings() {
        $this->field_settings = [
            'key_test' => [
                'type' => 'text',
                'label' => 'Llave pública de pruebas',
            ],

           'key_prod' => [
                'type' => 'text',
                'label' => 'Llave pública de producción',
            ],
            'key_priv_test' => [
                'type' => 'text',
                'label' => 'Llave privada de pruebas',
            ],
            'key_priv_prod' => [
                'type' => 'text',
                'label' => 'Llave privada de producción',
            ],
             'url_test' => [
                'type' => 'text',
                'label' => 'URL de pruebas',
            ],
           'url_prod' => [
                'type' => 'text',
                'label' => 'URL de producción',
            ],
            'test' => [
                'type' => 'checkbox',
                'label' => 'Test',
                'default' => '1'
            ],

        ];

    }



    public function confirm() {
        $data = request()->all();
        // $data = json_decode($dataPost);
        // dd($data->data->transaction->id);
        // $id_transaction = $data->data->transaction->id;
        $id_transaction = $data['id'];
        $params = $this->module->getSettings();
        $test = $params->test;
        $url = $test ? $params->url_test : $params->url_prod;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/transactions/' .$id_transaction);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        //     'Authorization: Bearer ' . $public_key));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        $result = curl_exec($ch);
        $data = json_decode($result); 
        $referenceCode = $data->data->reference;
        $dataTx['order_reference'] = $referenceCode;
        $dataTx['transaction_id'] = $data->data->id;
        // $dataTx['payer_name'] = $dataPost['nickname_buyer'];
        // $dataTx['payer_email'] = urldecode($dataPost['email_buyer']);
        $datePayment = $data->data->created_at;
        $datePayment = urldecode($datePayment);
        $datePayment = date('Y-m-d H:i:s', strtotime($datePayment));
        $dataTx['payment_date'] = $datePayment;
        $dataTx['amount'] = $data->data->amount_in_cents;
        $dataTx['currency'] = $data->data->currency;
        $dataTx['verified'] = 1;
        $amount = $data->data->amount_in_cents;
        $amount = number_format($amount, 1, '.', '');
        $currency = $data->data->currency;
        //Estado de la transacción - 4 para pago aprobado

        $stateTx = $data->data->status;
        //Retorna valores con la infromación de la transacción
        if ($stateTx == 'APPROVED') {
            $status = 2;
        }elseif($stateTx == 'DECLINED'){
            $status = 6;
        }else{
            $status = 1;
        }
        //Devolver la referencia del pedido, el estado y los datos de la transaccion
        return [
            'order' => $referenceCode,
            'status' => $status,
            'params' => $dataTx
        ];
    }

    public function verified($reference) {
        $params = $this->module->getSettings();
        $test = $params->test;
        $url = $test ? $params->url_test : $params->url_prod;
        $privad_key = $test ? $params->key_priv_test : $params->key_priv_prod;;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '/transactions?reference=' . $reference);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: application/json',
            'Authorization: Bearer ' . $privad_key));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        $result = curl_exec($ch);
        $data = json_decode($result); 
        if (!empty($data->data)) {
            $dataPost = $data->data[0];

            $referenceCode = $dataPost->reference;
            $dataTx['order_reference'] = $referenceCode;
            $dataTx['transaction_id'] = $dataPost->id;
            // $dataTx['payer_name'] = $dataPost['nickname_buyer'];
            // $dataTx['payer_email'] = urldecode($dataPost['email_buyer']);
            $datePayment = $dataPost->created_at;
            $datePayment = urldecode($datePayment);
            $datePayment = date('Y-m-d H:i:s', strtotime($datePayment));
            $dataTx['payment_date'] = $datePayment;
            $dataTx['amount'] = $dataPost->amount_in_cents;
            $dataTx['currency'] = $dataPost->currency;
            $amount = $dataPost->amount_in_cents;
            $amount = number_format($amount, 1, '.', '');
            $currency = $dataPost->currency;
            //Estado de la transacción - 4 para pago aprobado

            $stateTx = $dataPost->status;
            // dd($dataTx);
            //Retorna valores con la infromación de la transacción
            if ($stateTx == 'APPROVED') {
                $status = 2;
            }elseif($stateTx == 'DECLINED'){
                $status = 6;
            }else{
                $status = 1;
            }
            //Devolver la referencia del pedido, el estado y los datos de la transaccion
            return [
                'order' => $referenceCode,
                'status' => $status,
                'params' => $dataTx
            ];
        }else{
           return false;
        }
        
    }

}